class adb::configure::pm2 {
	file { '/etc/systemd/system/pm2.service':
		ensure => present,
		content => template('adb/pm2.service.erb')
	} ->
	exec { 'enable PM2 service':
		command => '/usr/bin/systemctl enable pm2'
	}
}