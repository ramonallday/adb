class adb::configure::projects {
	$install_path = "${adb::install_root}/${adb::install_dir}"
	$repos = $adb::projects
	$port = $adb::port

	file { 'ecosystem':
		ensure => file,
		path => "${install_path}/ecosystem.json",
		content => template("adb/pm2.ecosystem.erb")
	} 
	->
	exec { 'launch pm2':
		command => "pm2 start ecosystem.json",
		cwd     => $install_path,
		#user    => 'root',
		#path    => ['/usr/local/node/node-default/bin', '/bin', '/usr/bin'],
		path        => $::path,
		user        => $adb::deamon_user,
	    environment => ["HOME=${install_path}"],
	    group       => $adb::deamon_user,
		#creates     => '/file/created',
		#cwd         => '/path/to/run/from',
		#user        => 'user_to_run_as',
		#unless      => 'test param-that-would-be-true',
		#refreshonly => true,
	}
}