class adb::install::pm2 
(
	$pm2_version = $adb::pm2_version
) 
{
	package { 'pm2':
	  ensure   => 'present',
	  provider => 'npm',
	}
}