class adb::install::nginx {
	$repos = $adb::projects
	$port = $adb::port

	package { 'epel-release':
	 	ensure => installed,
	} ->
	package { 'nginx':
		ensure => installed,
	} ->
	file { '/etc/nginx/nginx.conf':
		ensure => file,
		content => template('adb/nginx.config.erb')
	} ->
	file { '/etc/nginx/conf.d/locations.conf':
		ensure => file,
		content => template('adb/nginx.locations.erb')
	} ->
	service { 'nginx':
		enable      => true,
		ensure      => running,
		#hasrestart => true,
		#hasstatus  => true,
		#require    => Class["config"],
	}
}