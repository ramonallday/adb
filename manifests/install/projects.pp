class adb::install::projects {
	$install_path = "${adb::install_root}/${adb::install_dir}"

	each ( $adb::projects ) | $repo | {
		# check if repo url has changed
		# if changed delete folder
		# initialize new repo

		#check if directory exists
		# exec { "check remote ${repo['name']}":
		# 	command  => "git remote get-url origin | grep -icv '${repo['source']}'",
		# 	cwd     => $install_path,
		# 	user    => 'root',
		# 	path    => $::path,
			
		# } 
		# exec { "check remote ${repo['name']}":
		# 	command  => "rm -rf ${repo['name']}",
		# 	cwd     => $install_path,
		# 	user    => 'root',
		# 	path    => $::path,
		# 	onlyif  => "[ -f ${repo['name']}/.git/config ] && cat ${repo['name']}/.git/config | grep -iq ${repo['source']}"
		# 		# check if remote url matches
		# 		# "cd ${install_path}/${repo['name']} && git remote get-url origin | grep -icv '${repo['source']}'"
		# }
		# notify { "running here ${repo['name']}":
		# } 
		vcsrepo { "${install_path}/${repo['name']}":
			ensure   => latest,
			provider => git,
			source   => $repo['source'],
			revision => 'master',
		} ->
		exec { "npm ${repo['name']}":
			command => "npm install",
			cwd     => "${install_path}/${repo['name']}",
			user    => 'root',
			path    => $::path,
		}
	}
}