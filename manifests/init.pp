class adb 
(
	$install_root    = '/opt',
  	$install_dir     = 'nodejs',
  	$node_dir        = '/usr/local/node/node-default',
  	$deamon_user     = 'nodejs',
	$pm2_version = 'latest',
	$port = 8000,
	$projects
) 
{
	$install_path = "${install_root}/${install_dir}"

	#create user
	group { $deamon_user:
		ensure  => present,
	} ->
	user { $deamon_user:
	    ensure     => present,
	    gid        => $deamon_user,
	    managehome => true,
	    shell      => '/bin/bash',
	    home       => $install_path,
	    groups     => [$deamon_user],
	    password   => "",
	    #require    => Group[$deamon_user]
	} ->
	file { $install_path:
	    ensure  => directory,
	    owner   => $deamon_user,
	    group   => $deamon_user,
	    mode    => '0750',
	    #require => [User[$deamon_user], Group[$deamon_user]]
	} ->
	class { 'adb::install::nginx': } ->
	class { 'adb::install::pm2': } ->
	#class { 'adb::configure::pm2': } ->
	class { 'adb::install::projects': }
	class { 'adb::configure::projects': }
}